# Advent of Code 2019

> A C++ implementation of the [Advent of Code](https://adventofcode.com/2019/day/2#part2) calendar challenges for 2019

The challenges for each day are implemented using C++.

## Usage
The code for each challenge must be compiled and built, which requires `cmake` and the `g++` compiler.

To compile, simply run:

```shell
$	mkdir build && cd $_
$	cmake ..
$	cmake --build .
```

A bash script is also provided in each directory to automate running those commands.

Most challenges provide an input, which must be saved to a text file, and the file name passed to the built program as an argument, e.g.

```shell
$	./build/Part1 input.txt
$	./build/Part2 input.txt
```

