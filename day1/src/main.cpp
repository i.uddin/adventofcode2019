#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <math.h>

using namespace std;

vector<int> readFile(string);
void saveFuelRequirements(vector<int>& moduleMasses, string outputFilename);
int sumFuelRequirements(vector<int>& moduleMasses, int (*fuelCalculator)(int, int));
int recursivelyCalcFuel(int mass, int totalFuel);

int main(int argc, char *argv[])
{
    vector<int> moduleMasses = readFile(argv[1]);
    // saveFuelRequirements(moduleMasses, argv[2]);
    int fuelSum = sumFuelRequirements(moduleMasses, &recursivelyCalcFuel);
    cout << "Fuel sum requirements: " << fuelSum << endl;
}

vector<int> readFile(string filename)
{
    string line;
    vector<int> lines;
    ifstream file(filename);
    while(getline(file, line))
    {
        lines.push_back(stoi(line));
    }
    return lines;
}

int calcFuelRequired(int mass)
{
    return round(mass / 3) - 2;
}

void saveFuelRequirements(vector<int>& moduleMasses, string outputFilename)
{
    ofstream outputFile(outputFilename);
    for (int i = 0; i < moduleMasses.size(); i++)
    {
        outputFile << calcFuelRequired(moduleMasses.at(i)) << endl;
    }
    outputFile.close();
}

int sumFuelRequirements(vector<int>& moduleMasses, int (*fuelCalculator)(int, int))
{
    int sum = 0;
    for (int i = 0; i < moduleMasses.size(); i++)
    {
        sum = sum + fuelCalculator(moduleMasses.at(i), 0);
    }
    return sum;
}

int recursivelyCalcFuel(int mass=0, int totalFuel=0)
{
    if (mass < 1)
    {
        return totalFuel;
    }
    int fuel = calcFuelRequired(mass);
    if (fuel < 0) {
        fuel = 0;
    }
    totalFuel = totalFuel + fuel;
    recursivelyCalcFuel(fuel, totalFuel);
}