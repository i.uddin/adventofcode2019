#! /bin/bash

if [ -d "build" ]
then
    cd build && rm -rf *
else
    mkdir build && cd $_
fi
cmake ..
cmake --build .
cd ..