#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <math.h>

using namespace std;

string readFile(string);
vector<int> splitNumbers(string& numbers, char delimiter);
void setProgramAlarm(vector<int>& numbers);
void runProgram(vector<int>& numbers);

int main(int argc, char *argv[])
{
    string intcodes = readFile(argv[1]);
    vector<int> numbers = splitNumbers(intcodes, ',');
    setProgramAlarm(numbers);
    runProgram(numbers);
    cout << "Position 0 is " << numbers.at(0) << endl;
}

string readFile(string filename)
{
    string line;
    vector<string> lines;
    ifstream file(filename);
    getline(file, line);
    return line;
}

vector<int> splitNumbers(string& numbers, char delimiter)
{
    vector<int> output;
    stringstream stream(numbers);
    string number;
    while(getline(stream, number, delimiter))
    {
        output.push_back(stoi(number));
    }
    return output;
}

void setProgramAlarm(vector<int>& numbers)
{
    numbers.at(1) = 12;
    numbers.at(2) = 2;
}

void runProgram(vector<int>& numbers)
{
    for (int i = 0; i < numbers.size() - 3; i = i + 4)
    {
        int opcode = numbers.at(i);
        int position1 = numbers.at(i + 1);
        int position2 = numbers.at(i + 2);
        int outputPosition = numbers.at(i + 3);
        int output;
        if (opcode == 1)
        {
            output = numbers.at(position1) + numbers.at(position2);
        } else if (opcode == 2) {
            output = numbers.at(position1) * numbers.at(position2);
        } else if (opcode == 99) {
            return;
        }
        numbers.at(outputPosition) = output;
    }
}