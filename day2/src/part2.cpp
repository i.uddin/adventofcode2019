#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <math.h>

using namespace std;

string readFile(string);
vector<int> splitNumbers(string& numbers, char delimiter);
vector<int> setProgramAlarm(vector<int> numbers, int noun, int verb);
vector<int> runProgram(vector<int> numbers);
vector<int> findInputs(vector<int>& numbers, int desiredOutput);


int main(int argc, char *argv[])
{
    string intcodes = readFile(argv[1]);
    vector<int> numbers = splitNumbers(intcodes, ',');
    vector<int> result = findInputs(numbers, stoi(argv[2]));
    cout << "Verb is: " << result.at(0) << endl;
    cout << "Noun is: " << result.at(1) << endl;
}

string readFile(string filename)
{
    string line;
    vector<string> lines;
    ifstream file(filename);
    getline(file, line);
    return line;
}

vector<int> splitNumbers(string& numbers, char delimiter)
{
    vector<int> output;
    stringstream stream(numbers);
    string number;
    while(getline(stream, number, delimiter))
    {
        output.push_back(stoi(number));
    }
    return output;
}

vector<int> setProgramAlarm(vector<int> numbers, int noun, int verb)
{
    numbers.at(1) = noun;
    numbers.at(2) = verb;
    return numbers;
}

vector<int> runProgram(vector<int> numbers)
{
    for (int i = 0; i < numbers.size() - 3; i = i + 4)
    {
        int opcode = numbers.at(i);
        int position1 = numbers.at(i + 1);
        int position2 = numbers.at(i + 2);
        int outputPosition = numbers.at(i + 3);
        int output;
        if (opcode == 1)
        {
            output = numbers.at(position1) + numbers.at(position2);
        } else if (opcode == 2) {
            output = numbers.at(position1) * numbers.at(position2);
        } else if (opcode == 99) {
            return numbers;
        }
        numbers.at(outputPosition) = output;
    }
    return numbers;
}

vector<int> findInputs(vector<int>& numbers, int desiredOutput)
{
    int iterations = 0;
    for (int noun = 0; noun < 100; noun++)
    {
        for (int verb = 0; verb < 100; verb++)
        {
            vector<int> initialisedNumbers = setProgramAlarm(numbers, noun, verb);
            vector<int> output = runProgram(initialisedNumbers);
            iterations++;
            if (output.at(0) == desiredOutput)
            {
                cout << "Ended after " << iterations << " iterations" << endl;
                return vector<int> {noun, verb};
            }
        }
    }
    return vector<int> {0, 0};
}