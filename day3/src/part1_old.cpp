#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <math.h>
#include <chrono>

using namespace std;
using namespace std::chrono;

using Matrix = vector<vector<int>>;

class Point {
public:
    int x;
    int y;

    Point(int x_coord, int y_coord): x(x_coord), y(y_coord) {}
    Point() : x(0), y(0) {}
};

class Wire {
private:
    vector<string> instructions;
    vector<Point> points;
    Matrix matrix;
    int width;
    int height;
    int yOffset;
    int xOffset;

    vector<Point> interpolatePointsFromInstruction(Point point, char direction, int magnitude)
    {
        vector<Point> interpolatedPoints;
        Point extraPoint(0, 0);
        switch (direction) {
            case 'U':
                for (int i = 1; i < magnitude; i++)
                {
                    extraPoint = Point(point.x, point.y + i);
                    interpolatedPoints.push_back(extraPoint);
                }
                break;
            case 'D':
                for (int i = 1; i < magnitude; i++)
                {
                    extraPoint = Point(point.x, point.y - i);
                    interpolatedPoints.push_back(extraPoint);
                }
                break;
            case 'R':
                for (int i = 1; i < magnitude; i++)
                {
                    extraPoint = Point(point.x + i, point.y);
                    interpolatedPoints.push_back(extraPoint);
                }
                break;
            case 'L':
                for (int i = 1; i < magnitude; i++)
                {
                    extraPoint = Point(point.x - i, point.y);
                    interpolatedPoints.push_back(extraPoint);
                }
                break;
            default:
                break;
        }
        return interpolatedPoints;
    }

    vector<Point> convertToPoints()
    {
        Point currentPoint(0, 0);
        vector<Point> wirePoints;
        vector<Point> interpolatedPoints;
        int minX = 0, maxX = 0, minY = 0, maxY = 0;
        for (string instruction: instructions)
        {
            char direction = instruction.at(0);
            int magnitude = stoi(instruction.substr(1, instruction.size() - 1));
            interpolatedPoints = interpolatePointsFromInstruction(currentPoint, direction, magnitude);
            currentPoint = interpolatedPoints.at(interpolatedPoints.size() - 1);
            // cout << currentPoint.x << endl;
            // cout << currentPoint.y << endl;
            for (Point point: interpolatedPoints)
            {
                wirePoints.push_back(point);
            }
            interpolatedPoints.clear();
            minX = currentPoint.x < minX ? currentPoint.x : minX;
            maxX = currentPoint.x > maxX ? currentPoint.x : maxX;
            minY = currentPoint.y < minY ? currentPoint.y : minY;
            maxY = currentPoint.y > maxY ? currentPoint.y : maxY;
        }
        width = abs(maxX - minX) + 1;
        height = abs(maxY - minY) + 1;
        // cout << minX << endl;
        // cout << minX << endl;
        // cout << minY << endl;
        // cout << maxY << endl;
        // cout << width << endl;
        // cout << height << endl;
        if (minX < 0)
        {
            xOffset = abs(minX);
        }
        if (minY < 0)
        {
            yOffset = abs(minY);
        }
        return wirePoints;
    }

    

public:
    Wire(
        vector<string> wireInstructions
    ): 
    instructions(wireInstructions), xOffset(0), yOffset(0)
    {
        points = convertToPoints();
    }

    Matrix getMatrix()
    {
        return matrix;
    }

    vector<Point> getPoints()
    {
        return points;
    }

    void buildMatrixFromPoints()
    {
        // cout << width << endl;
        // cout << height << endl;
        // cout << xOffset << endl;
        // cout << yOffset << endl;
        matrix = Matrix(height, vector<int>(width, 0));
        for (Point p: points)
        {
            matrix.at(p.y + yOffset).at(p.x + xOffset) = 1;
        }
    }

    vector<Point> findIntersections(Matrix toCompare)
    {
        vector<Point> intersections;
        Point intersection;
        for (Point p: points)
        {
            int value = 0;
            try {
                value = toCompare.at(p.y + yOffset).at(p.x + xOffset);
            }
            catch (const std::out_of_range& e)
            {
                continue;
            }
            if (value == 1)
            {
                intersection = Point(p.x + xOffset, p.y + yOffset);
                intersections.push_back(intersection);
            }
        }
        return intersections;
    }
};


vector<string> readFile(string);
vector<string> parseWireInstructions(string& wireInstructions, char delimiter);
vector<vector<string>> parseWires(vector<string> fileContents);
int manhattanDist(Point p1, Point p2);
int findClosestDist(vector<Point> points, int (*distFunc)(Point, Point));


int main(int argc, char *argv[])
{
    vector<string> fileContents = readFile(argv[1]);
    vector<vector<string>> wires = parseWires(fileContents);
    Wire wire1(wires.at(0));
    wire1.buildMatrixFromPoints();
    Wire wire2(wires.at(1));

    // Need to translate both matrices by same amount thats why
    // closest distance is too high :/
    vector<Point> intersections = wire2.findIntersections(wire1.getMatrix());
    int closestDist = findClosestDist(intersections, &manhattanDist);
    cout << closestDist << endl;
}


vector<string> readFile(string filename)
{
    string line;
    vector<string> lines;
    ifstream file(filename);
    while(getline(file, line))
    {
        lines.push_back(line);
    }
    return lines;
}


vector<string> parseWireInstructions(string& wireInstructions, char delimiter)
{
    vector<string> wire;
    stringstream stream(wireInstructions);
    string instruction;
    while(getline(stream, instruction, delimiter))
    {
        wire.push_back(instruction);
    }
    return wire;
}


vector<vector<string>> parseWires(vector<string> fileContents)
{
    vector<vector<string>> wires;
    for (int i = 0; i < fileContents.size(); i++)
    {
        vector<string> wire = parseWireInstructions(fileContents.at(i), ',');
        wires.push_back(wire);
    }
    return wires;
}


int manhattanDist(Point p1, Point p2)
{
    return abs(p1.x - p2.x) + abs(p1.y - p2.y);
}

int findClosestDist(vector<Point> points, int (*distFunc)(Point, Point))
{
    int closestDist = 100000;
    Point origin(0, 0);
    for (Point p: points){
        int dist = distFunc(origin, p);
        if (dist < closestDist)
        {
            closestDist = dist;
        }
    }
    return closestDist;
}