#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <math.h>
#include <chrono>
#include <map>

using namespace std;
using namespace std::chrono;

using Matrix = vector<vector<int>>;

class Point {
public:
    int x;
    int y;

    Point(int x_coord, int y_coord): x(x_coord), y(y_coord) {}
    Point() : x(0), y(0) {}
};

vector<string> readFile(string);
vector<string> split(string& input, char delimiter);
vector<vector<string>> parseWires(vector<string> fileContents);
int manhattanDist(Point p1, Point p2);
int findClosestDist(vector<Point> points, int (*distFunc)(Point, Point));



class Wire {
private:
    vector<string> instructions;
    vector<string> coordinates;
    map<string, int> coordinateDictionary;

    vector<Point> interpolatePointsFromInstruction(Point point, char direction, int magnitude)
    {
        vector<Point> interpolatedPoints;
        Point extraPoint(0, 0);
        switch (direction) {
            case 'U':
                for (int i = 1; i < magnitude + 1; i++)
                {
                    extraPoint = Point(point.x, point.y + i);
                    interpolatedPoints.push_back(extraPoint);
                }
                break;
            case 'D':
                for (int i = 1; i < magnitude + 1; i++)
                {
                    extraPoint = Point(point.x, point.y - i);
                    interpolatedPoints.push_back(extraPoint);
                }
                break;
            case 'R':
                for (int i = 1; i < magnitude + 1; i++)
                {
                    extraPoint = Point(point.x + i, point.y);
                    interpolatedPoints.push_back(extraPoint);
                }
                break;
            case 'L':
                for (int i = 1; i < magnitude + 1; i++)
                {
                    extraPoint = Point(point.x - i, point.y);
                    interpolatedPoints.push_back(extraPoint);
                }
                break;
            default:
                break;
        }
        return interpolatedPoints;
    }

    vector<string> callAllCoordinates()
    {
        Point currentPoint(0, 0);
        vector<string> wireCoords;
        vector<Point> interpolatedPoints;
        for (string instruction: instructions)
        {
            char direction = instruction.at(0);
            int magnitude = stoi(instruction.substr(1, instruction.size() - 1));
            interpolatedPoints = interpolatePointsFromInstruction(currentPoint, direction, magnitude);
            currentPoint = interpolatedPoints.at(interpolatedPoints.size() - 1);
            for (Point point: interpolatedPoints)
            {
                wireCoords.push_back(to_string(point.x) + ", " + to_string(point.y));
            }
            interpolatedPoints.clear();
        }
        return wireCoords;
    }

public:
    Wire(
        vector<string> wireInstructions
    ): 
    instructions(wireInstructions)
    {
        coordinates = callAllCoordinates();
    }

    vector<string> getCoords()
    {
        return coordinates;
    }

    map<string, int> getCoordDictionary()
    {
        return coordinateDictionary;
    }

    void buildCoordMap(int (*distFunc)(Point, Point))
    {
        Point origin(0, 0);
        for (string c: coordinates)
        {
            vector<string> coords = split(c, ',');
            int x = stoi(coords.at(0));
            int y = stoi(coords.at(1));
            int dist = distFunc(origin, Point(x, y));
            coordinateDictionary.insert_or_assign(c, dist);
        }
    }

    int findClosestIntersection(map<string, int> coordDict)
    {
        vector<Point> intersections;
        int closestDist = 100000000;
        for (string c: coordinates)
        {
            if ( coordDict.find(c) == coordDict.end() )
            {
                continue;
            } else {
                int dist = coordDict.at(c);
                if ( dist < closestDist )
                {
                    closestDist = dist;
                }
            }
        }
        return closestDist;
    }

    int getStepsToCoord(string coord)
    {
        int steps = 0;
        for (int i = 0; i < coordinates.size(); i++)
        {
            // Incremented first as it is number of steps inclusive
            steps++;
            string currentCoord = coordinates.at(i);
            if ( currentCoord == coord )
            {
                break;
            }
        }
        return steps;
    }

    int fewestCombinedStepsToIntsctn(map<string, int> coordDict, Wire& partnerWire)
    {
        int lowestCombinedSteps = 100000000;
        for (string c: coordinates) {
            if ( coordDict.find(c) == coordDict.end() )
            {
                continue;
            } else {
                // Intersection found
                int currentWireSteps = getStepsToCoord(c);
                int partnerWireSteps = partnerWire.getStepsToCoord(c);
                int combinedSteps = currentWireSteps + partnerWireSteps;
                if ( combinedSteps < lowestCombinedSteps )
                {
                    lowestCombinedSteps = combinedSteps;
                }
            }
        }
        return lowestCombinedSteps;
    }
};





int main(int argc, char *argv[])
{
    vector<string> fileContents = readFile(argv[1]);
    vector<vector<string>> wires = parseWires(fileContents);
    Wire wire1(wires.at(0));
    Wire wire2(wires.at(1));
    wire1.buildCoordMap(&manhattanDist);
    map<string, int> wire1CoordDict = wire1.getCoordDictionary();
    int fewestCombSteps = wire2.fewestCombinedStepsToIntsctn(wire1CoordDict, wire1);
    cout << fewestCombSteps << endl;
}


vector<string> readFile(string filename)
{
    string line;
    vector<string> lines;
    ifstream file(filename);
    while(getline(file, line))
    {
        lines.push_back(line);
    }
    return lines;
}


vector<string> split(string& input, char delimiter)
{
    vector<string> output;
    stringstream stream(input);
    string element;
    while( getline(stream, element, delimiter))
    {
        output.push_back(element);
    }
    return output;
}


vector<vector<string>> parseWires(vector<string> fileContents)
{
    vector<vector<string>> wires;
    for (int i = 0; i < fileContents.size(); i++)
    {
        vector<string> wire = split(fileContents.at(i), ',');
        wires.push_back(wire);
    }
    return wires;
}


int manhattanDist(Point p1, Point p2)
{
    return abs(p1.x - p2.x) + abs(p1.y - p2.y);
}
