#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <math.h>
#include <chrono>
#include <map>

using namespace std;
using namespace std::chrono;

vector<int> toVectorInt(int number);
int findNOfPasswords(int upper, int lower);

int main(int argc, char *argv[])
{
    int lower = stoi(argv[1]);
    int upper = stoi(argv[2]);
    int number = findNOfPasswords(upper, lower);
    cout << number << endl;
   
}

vector<int> toVectorInt(int number)
{
    vector<int> output;
    string input = to_string(number);
    for (int i = 0; i < input.size(); i++)
    {
        output.push_back((int)(input.at(i) - '0'));
    }
    return output;
}

bool twoAdjacentDigitsSame(vector<int> password)
{
    bool conditionMet = false;
    for (int i = 0; i < password.size(); i++)
    {
        if ( i == 0 )
        {
            continue;
        }
        if ( password.at(i) == password.at(i - 1) )
        {
            conditionMet = true;
            return conditionMet;
        }
    }
    return conditionMet;
}

bool lToRIncreasing(vector<int> password)
{
    bool conditionMet = true;
    for (int i = 0; i < password.size(); i++)
    {
        if ( i == 0 )
        {
            continue;
        }
        if ( password.at(i) < password.at(i - 1) )
        {
            conditionMet = false;
            return conditionMet;
        }
    }
    return conditionMet;
}

int findNOfPasswords(int upper, int lower)
{
    int nOfPasswords = 0;
    vector<int> password = toVectorInt(lower);
    for (int p = lower; p < upper; p++)
    {
        vector<int> newPassword = toVectorInt(p);
        bool condition1 = twoAdjacentDigitsSame(newPassword);
        bool condition2 = lToRIncreasing(newPassword);
        if ( condition1 && condition2 )
        {
            nOfPasswords++;
        }
    }
    return nOfPasswords;
}