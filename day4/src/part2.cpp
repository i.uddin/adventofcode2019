#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <math.h>
#include <chrono>
#include <map>

using namespace std;
using namespace std::chrono;

vector<int> toVectorInt(int number);
int findNOfPasswords(int upper, int lower);
bool lToRIncreasing(vector<int> password);
bool onlyTwoAdjacentDigitsSame(vector<int> password);

int main(int argc, char *argv[])
{
    int lower = stoi(argv[1]);
    int upper = stoi(argv[2]);
    int number = findNOfPasswords(lower, upper);
    cout << number << endl;
   
}

vector<int> toVectorInt(int number)
{
    vector<int> output;
    string input = to_string(number);
    for (int i = 0; i < input.size(); i++)
    {
        output.push_back((int)(input.at(i) - '0'));
    }
    return output;
}

bool twoAdjacentDigitsSame(vector<int> password)
{
    bool conditionMet = false;
    int digit;
    for (int i = 0; i < password.size(); i++)
    {
        if ( i == 0 )
        {
            continue;
        }
        if ( password.at(i) == password.at(i - 1) )
        {
            conditionMet = true;
            digit = password.at(i);
            return digit;
        }
    }
    return conditionMet;
}

bool onlyTwoAdjacentDigitsSame(vector<int> password)
{
    bool conditionMet = false;
    map<int, int> digits;
    for (int i = 0; i < password.size(); i++ )
    {
        map<int, int>::iterator it = digits.find(password.at(i));
        if ( it == digits.end() )
        {
            digits.insert(pair<int, int>(password.at(i), 1));
        } else {
            it->second++;
        }
    }
    int doubleCount = 0;
    int groupCount = 0;
    for (map<int, int>::iterator it = digits.begin(); it != digits.end(); ++it)
    {
        if ( it->second == 2 )
        {
            doubleCount++;
        } else if ( it->second > 2 ) {
            groupCount++;
        }
    }
    if ( (doubleCount >= groupCount) && (doubleCount > 0) )
    {
        conditionMet = true;
    }
    return conditionMet;
}

bool lToRIncreasing(vector<int> password)
{
    bool conditionMet = true;
    for (int i = 0; i < password.size(); i++)
    {
        if ( i == 0 )
        {
            continue;
        }
        if ( password.at(i) < password.at(i - 1) )
        {
            conditionMet = false;
            return conditionMet;
        }
    }
    return conditionMet;
}

int findNOfPasswords(int lower, int upper)
{
    int nOfPasswords = 0;
    vector<int> password = toVectorInt(lower);
    for (int p = lower; p < upper; p++)
    {
        vector<int> newPassword = toVectorInt(p);
        bool condition1 = onlyTwoAdjacentDigitsSame(newPassword);
        bool condition2 = lToRIncreasing(newPassword);
        if ( condition1 && condition2 )
        {
            nOfPasswords++;
        }
    }
    return nOfPasswords;
}